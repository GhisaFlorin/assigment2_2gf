﻿using System;
using ProxyLibrary;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetRemoting
{


    class CarServiceServer : MarshalByRefObject, ICarService
    {
        public double ComputeSelPrice(Car car)
        {
            return car.purchasingPrice - (car.purchasingPrice / 7) * (2015 - car.year);
        }

        public double ComputeTax(Car car)
        {
            if (car.engineSize <= 0)
            {
                throw new Exception();
            }
            int sum = 8;
            if (car.engineSize > 1601) sum = 18;
            if (car.engineSize > 2001) sum = 72;
            if (car.engineSize > 2601) sum = 144;
            if (car.engineSize > 3001) sum = 290;
            return car.engineSize / 200.0 * sum;
        }
    }
}
