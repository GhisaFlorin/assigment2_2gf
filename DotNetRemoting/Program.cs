﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using ProxyLibrary;

namespace DotNetRemoting
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TcpServerChannel channel = new TcpServerChannel(9999);
                ChannelServices.RegisterChannel(new TcpClientChannel(),false);
                RemotingConfiguration.RegisterWellKnownServiceType(typeof(CarServiceServer),
                    "ICarService", WellKnownObjectMode.SingleCall);
                Console.WriteLine("The server is running");
                Console.ReadLine();
            }
            catch 
            {
                Console.WriteLine("Can't start Server");
                Console.ReadKey();
            }
        }
    }
}
