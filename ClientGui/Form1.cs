﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProxyLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;

namespace ClientGui
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            TcpClientChannel channel = new TcpClientChannel();
            ChannelServices.RegisterChannel(channel, false);
            InitializeComponent();
        }

        private void taxButton_Click(object sender, EventArgs e)
        {
            try
            {
               
                ICarService iCarService = (ICarService)(Activator.GetObject(typeof(ICarService), "tcp://localhost:9999/ICarService"));
                Console.WriteLine("The computations:");
                Car c = new Car() { engineSize = Convert.ToInt32(engineTextBox.Text), purchasingPrice= Convert.ToInt32(priceTextBox.Text), year = Convert.ToInt32(yearTextBox.Text) };           
                resultLabel.Text=iCarService.ComputeTax(c).ToString();

            }
            catch (Exception ex)
            {
                resultLabel.Text = "Eroare: " + ex;
            }
        }

        private void sellButton_Click(object sender, EventArgs e)
        {
            try
            {            
                ICarService iCarService = (ICarService)(Activator.GetObject(typeof(ICarService), "tcp://localhost:9999/ICarService"));
                Console.WriteLine("The computations:");
                Car c = new Car() { engineSize = Convert.ToInt32(engineTextBox.Text), purchasingPrice = Convert.ToInt32(priceTextBox.Text), year = Convert.ToInt32(yearTextBox.Text) };
                resultLabel.Text = iCarService.ComputeSelPrice(c).ToString();

            }
            catch (Exception ex)
            {
                resultLabel.Text = "Eroare: " + ex;
            }

        }
    }
}
