﻿namespace ClientGui
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.yearTextBox = new System.Windows.Forms.TextBox();
            this.engineTextBox = new System.Windows.Forms.TextBox();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.taxButton = new System.Windows.Forms.Button();
            this.sellButton = new System.Windows.Forms.Button();
            this.year = new System.Windows.Forms.Label();
            this.engineSize = new System.Windows.Forms.Label();
            this.purchasePrice = new System.Windows.Forms.Label();
            this.reultTextLabel = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // yearTextBox
            // 
            this.yearTextBox.Location = new System.Drawing.Point(74, 12);
            this.yearTextBox.Name = "yearTextBox";
            this.yearTextBox.Size = new System.Drawing.Size(100, 20);
            this.yearTextBox.TabIndex = 0;
            // 
            // engineTextBox
            // 
            this.engineTextBox.Location = new System.Drawing.Point(74, 54);
            this.engineTextBox.Name = "engineTextBox";
            this.engineTextBox.Size = new System.Drawing.Size(100, 20);
            this.engineTextBox.TabIndex = 1;
            // 
            // priceTextBox
            // 
            this.priceTextBox.Location = new System.Drawing.Point(74, 95);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.Size = new System.Drawing.Size(100, 20);
            this.priceTextBox.TabIndex = 2;
            // 
            // taxButton
            // 
            this.taxButton.Location = new System.Drawing.Point(192, 30);
            this.taxButton.Name = "taxButton";
            this.taxButton.Size = new System.Drawing.Size(114, 23);
            this.taxButton.TabIndex = 3;
            this.taxButton.Text = "Calculate Tax";
            this.taxButton.UseVisualStyleBackColor = true;
            this.taxButton.Click += new System.EventHandler(this.taxButton_Click);
            // 
            // sellButton
            // 
            this.sellButton.Location = new System.Drawing.Point(192, 74);
            this.sellButton.Name = "sellButton";
            this.sellButton.Size = new System.Drawing.Size(114, 23);
            this.sellButton.TabIndex = 4;
            this.sellButton.Text = "Calculate Sell Price";
            this.sellButton.UseVisualStyleBackColor = true;
            this.sellButton.Click += new System.EventHandler(this.sellButton_Click);
            // 
            // year
            // 
            this.year.AutoSize = true;
            this.year.Location = new System.Drawing.Point(12, 15);
            this.year.Name = "year";
            this.year.Size = new System.Drawing.Size(29, 13);
            this.year.TabIndex = 5;
            this.year.Text = "Year";
            // 
            // engineSize
            // 
            this.engineSize.AutoSize = true;
            this.engineSize.Location = new System.Drawing.Point(7, 57);
            this.engineSize.Name = "engineSize";
            this.engineSize.Size = new System.Drawing.Size(61, 13);
            this.engineSize.TabIndex = 6;
            this.engineSize.Text = "Engine size";
            // 
            // purchasePrice
            // 
            this.purchasePrice.AutoSize = true;
            this.purchasePrice.Location = new System.Drawing.Point(12, 102);
            this.purchasePrice.Name = "purchasePrice";
            this.purchasePrice.Size = new System.Drawing.Size(31, 13);
            this.purchasePrice.TabIndex = 7;
            this.purchasePrice.Text = "Price";
            // 
            // reultTextLabel
            // 
            this.reultTextLabel.AutoSize = true;
            this.reultTextLabel.Location = new System.Drawing.Point(71, 168);
            this.reultTextLabel.Name = "reultTextLabel";
            this.reultTextLabel.Size = new System.Drawing.Size(40, 13);
            this.reultTextLabel.TabIndex = 8;
            this.reultTextLabel.Text = "Result:";
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Location = new System.Drawing.Point(139, 168);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(0, 13);
            this.resultLabel.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 265);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.reultTextLabel);
            this.Controls.Add(this.purchasePrice);
            this.Controls.Add(this.engineSize);
            this.Controls.Add(this.year);
            this.Controls.Add(this.sellButton);
            this.Controls.Add(this.taxButton);
            this.Controls.Add(this.priceTextBox);
            this.Controls.Add(this.engineTextBox);
            this.Controls.Add(this.yearTextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox yearTextBox;
        private System.Windows.Forms.TextBox engineTextBox;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.Button taxButton;
        private System.Windows.Forms.Button sellButton;
        private System.Windows.Forms.Label year;
        private System.Windows.Forms.Label engineSize;
        private System.Windows.Forms.Label purchasePrice;
        private System.Windows.Forms.Label reultTextLabel;
        private System.Windows.Forms.Label resultLabel;
    }
}

