﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyLibrary
{
    public interface ICarService
    {
         double ComputeTax(Car car);
         double ComputeSelPrice(Car car);
    }
}
