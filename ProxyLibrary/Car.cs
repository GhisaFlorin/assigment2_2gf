﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProxyLibrary
{
    [Serializable]
    public class Car
    {
        public int year { get; set; }
        public int engineSize { get; set; }
        public int purchasingPrice { get; set; }

    }
}
